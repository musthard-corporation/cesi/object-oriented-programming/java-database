package com.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
    private static final String url = "jdbc:postgresql://localhost/products";
    private static final String user = "postgres";
    private static final String password = "postgres";

    public static void main(String[] args) throws Exception {
        Connection conn = DriverManager.getConnection(url, user, password);
        printAllBags(conn);
        // createBag(conn, 99, "model", "color", false, false);
        // updateBag(conn, 7, 99, "model", "color", false, false);
        // deleteBag(conn, 7);
    }

    public static void printAllBags(Connection conn) {
        // try (...) {} revient à faire un try classique 
        // mais il s'occupe de ".close()" les variables créées entre parenthèses
        try (Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * from bags")) {
            while (rs.next()) {
                System.out.println("New line :");
                System.out.format(" - id: %d%n", rs.getInt("id"));
                System.out.format(" - price: %f%n", rs.getFloat("price"));
                System.out.format(" - model: %s%n", rs.getString("model"));
                System.out.format(" - color: %s%n", rs.getString("color"));
                System.out.format(" - with_handle: %s%n", rs.getString("with_handle"));
                System.out.format(" - shoulder_strap: %s%n", rs.getString("shoulder_strap"));

            }
        } catch (SQLException e) {
            System.err.println("An error occured while printing bags");
            e.printStackTrace();
        }
    }

    public static void createBag(Connection conn, float price, String model, String color, boolean withHandle,
            boolean shoulderStrap) {
        try (PreparedStatement pstmt = conn.prepareStatement(
                "INSERT INTO bags(price, model, color, with_handle, shoulder_strap) VALUES (?, ?, ?, ?, ?)")) {
            pstmt.setFloat(1, price);
            pstmt.setString(2, model);
            pstmt.setString(3, color);
            pstmt.setBoolean(4, withHandle);
            pstmt.setBoolean(5, shoulderStrap);
            int rowCount = pstmt.executeUpdate();
            if (rowCount > 0) {
                System.out.format("Successfully create %d bag(s)%n", rowCount);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void updateBag(Connection conn, int id, float price, String model, String color, boolean withHandle,
            boolean shoulderStrap) {
        try (PreparedStatement pstmt = conn.prepareStatement(
                "Update bags SET price = ?, model = ?, color = ?, with_handle = ?, shoulder_strap = ? WHERE id = ?")) {
            pstmt.setFloat(1, price);
            pstmt.setString(2, model);
            pstmt.setString(3, color);
            pstmt.setBoolean(4, withHandle);
            pstmt.setBoolean(5, shoulderStrap);
            pstmt.setInt(6, id);
            int rowCount = pstmt.executeUpdate();
            if (rowCount > 0) {
                System.out.format("Successfully update %d bag(s)%n", rowCount);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void deleteBag(Connection conn, int id) {
        try (PreparedStatement pstmt = conn.prepareStatement(
                "Update bags SET price = ?, model = ?, color = ?, with_handle = ?, shoulder_strap = ? WHERE id = ?")) {
            pstmt.setInt(1, id);
            int rowCount = pstmt.executeUpdate();
            if (rowCount > 0) {
                System.out.format("Successfully delete %d bag(s)%n", rowCount);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
